﻿using System;
using System.Collections.Generic;
using Renci.SshNet;

namespace DBMC
{
    public class sFTP
    {
        public static string SendFile(string host, string user, string pass, string fileName)
        {
            string output = null;
            try
            {
                var connectionInfo = new ConnectionInfo(host, "sftp", new PasswordAuthenticationMethod(user, pass));
                // Upload File
                using (var sftp = new SftpClient(connectionInfo))
                {
                    sftp.Connect();
                    //sftp.ChangeDirectory("/MyFolder");
                    using (var uplfileStream = System.IO.File.OpenRead(fileName))
                    {
                        sftp.UploadFile(uplfileStream, fileName, true);
                    }
                    sftp.Disconnect();
                }
            }
            catch (Exception e)
            {
                output = "<div class='alert - danger'>" + e.Message + "</div>";
            }
            return output;
        }

        public static string GetFile(string host, string user, string pass, string fileName)
        {
            string output = null;
            try
            {
                var connectionInfo = new ConnectionInfo(host, "sftp", new PasswordAuthenticationMethod(user, pass));
                using (var sftp = new SftpClient(connectionInfo))
                {
                    sftp.Connect();
                    using (var dwnfileStream = System.IO.File.OpenRead(fileName))
                    {
                        sftp.DownloadFile(fileName, dwnfileStream);
                    }
                    sftp.Disconnect();
                }
            }
            catch (Exception e)
            {
                output = "<div class='alert - danger'>" + e.Message + "</div>";
            }
            return output;
        }

        public static List<string> ListFile(string host, string user, string pass, string dir=null)
        {
            List<string> output = new List<string>();
            try
            {
                //var files = new List<String>();
                var connectionInfo = new ConnectionInfo(host, user, new PasswordAuthenticationMethod(user, pass));
                using (var sftp = new SftpClient(connectionInfo))
                {
                    sftp.Connect();
                    //sftp.ChangeDirectory("/");
                    if (dir is null)
                    {
                        dir = "";
                    }
                    //ListDirectory(sftp, dir, ref files);
                    foreach (var entry in sftp.ListDirectory(dir))
                    {
                        output.Add(entry.FullName);
                    }
                    sftp.Disconnect();
                }
            }
            catch (Exception e)
            {
                output.Add(string.Format("*** ERROR in sFTP.ListFile: {0}", e.Message));
            }
            return output;
        }

        public void ListDirectory(SftpClient client, String dirName, ref List<String> files)
        {
            foreach (var entry in client.ListDirectory(dirName))
            {
                if (entry.IsDirectory)
                {
                    ListDirectory(client, entry.FullName, ref files);
                }
                else
                {
                    files.Add(entry.FullName);
                }
            }
        }
    }
}
