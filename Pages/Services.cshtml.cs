using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace DBMC
{
    public class ServicesModel : PageModel
    {
        public string Message { get; set; }
        public string sfuser { get; set; }
        public string conn { get; set; }
        public List<string> files { get; set; }
        [BindProperty(SupportsGet = true)]
        public string InputSearch { get; set; }
        public IConfiguration Config { get; }
        public ServicesModel(IConfiguration config)
        {
            Config = config;
        }
        public void OnGet()
        {
            conn = Config["ConnectionStrings:Main"];
            sfuser = Config["sftp_server:User"] + '@' + Config["sftp_server:Host"];
        }

        public void fillServices()
        {
            System.ServiceProcess.ServiceController services = new System.ServiceProcess.ServiceController();
            if ((services.Status.Equals(System.ServiceProcess.ServiceControllerStatus.Stopped)) || 
                (services.Status.Equals(System.ServiceProcess.ServiceControllerStatus.StopPending)))
                services.Start();
            else    
                services.Stop();
        }
    }
}
