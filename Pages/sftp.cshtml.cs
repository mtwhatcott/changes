﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace DBMC.Pages
{
    public class sftpModel : PageModel
    {
        public string Message { get; set; }
        public string sfuser { get; set; }
        public List<string> files { get; set; }
        public string conn { get; set; }
        [BindProperty (SupportsGet =true)]
        public string InputSearch { get; set; }
        private readonly IConfiguration Config;

        public sftpModel(IConfiguration config)
        {
            this.Config = config;
        }
        public void OnGet()
        {
            conn = Config["ConnectionStrings:Main"];
            sfuser = Config["sftp_server:User"] + '@' + Config["sftp_server:Host"];
            files = sFTP.ListFile(Config["sftp_server:Host"], Config["sftp_server:User"], Config["sftp_server:Pass"], InputSearch);
        }
    }
}