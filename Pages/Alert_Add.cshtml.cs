﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace DBMC.Pages
{
    public class Alert_AddModel : PageModel
    {
        public string Message { get; set; }
        public string sfuser { get; set; }
        public string conn { get; set; }
        private readonly IConfiguration Config;
        public Alert_AddModel(IConfiguration config)
        {
            this.Config = config;
        }

        public void OnGet()
        {
            conn = Config["ConnectionStrings:Main"];
            sfuser = Config["sftp_server:User"] + '@' + Config["sftp_server:Host"];
        }
    }
}