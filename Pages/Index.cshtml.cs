﻿using System;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace DBMC.Pages
{
    public class IndexModel : PageModel
    {
        public string Message { get; private set; }
        public string Tab1 { get; private set; }
        public string Tab2 { get; private set; }
        public string Tab3 { get; private set; }
        public string Tab4 { get; private set; }
        public string Tab5 { get; private set; }
        private readonly IConfiguration config;

        public string cs { get; set; }
        public string user = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper();
        public string userid { get; set; }
        public IndexModel(IConfiguration config)
        {
            this.config = config;
        }

        public void OnGet()
        {
            try
            {
                cs = config["ConnectionStrings:Main"];
                userid = Global_Dash.GetUserID(cs, user);
                fillTabs();
            } catch (Exception e)
            {
                Message = string.Format("*** ERRROR 100 in page index.aspx:\r\n {0}", e.Message);
            }
        }

        public void fillTabs()
        {
            try
            {
                string sql = null;
                sql = "sp_GetMessagesCount '{0}'";
                Tab1 = getTabs(cs, string.Format(sql, userid), "Total User Messages");

                sql = $@"
SELECT COUNT(*) 
FROM vUsers
";
                Tab2 += getTabs(cs, sql, "Total Users");

                sql = "sp_GetTasksCount '{0}'";
                Tab3 += getTabs(cs, string.Format(sql, userid), "Total User Tasks");

                sql = "sp_GetAlertsCount '{0}'";
                Tab4 += getTabs(cs, string.Format(sql, userid), "Total User Alerts");
            }
            catch (Exception e)
            {
                Message = string.Format("*** ERRROR 101 in page index.aspx:\r\n {0}", e.Message);
            }
        }
        public string getTabs(string cs, string sql, string title)
        {
            string output = null;
            try
            {
                string count = "0";

                output = "{0} {1}";
                count = DCL.Data.GetData(cs, sql);

                object[] args = new object[] { count, title };
                output = string.Format(output, args);
            }
            catch (Exception e)
            {
                Message = string.Format("*** ERRROR 102 in page index.aspx:\r\n {0}", e.Message);
            }
            return output;
        }
    }
}
