﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace DBMC.Pages
{
    public class AlertsModel : PageModel
    {
        public string Message { get; set; }
        public string sfuser { get; set; }
        public string conn { get; set; }
        public List<string> files { get; set; }
        [BindProperty(SupportsGet = true)]
        public string InputSearch { get; set; }
        public IConfiguration Config { get; }

        public AlertsModel(IConfiguration config)
        {
            Config = config;
        }
        public void OnGet()
        {
            conn = Config["ConnectionStrings:Main"];
            sfuser = Config["sftp_server:User"] + '@' + Config["sftp_server:Host"];
        }
    }
}