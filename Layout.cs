﻿using System;

namespace DBMC
{
    public class Layout
    {
        public static string RenderMenu(string mnuLoc, string constr)
        {
            string output = "";
            try
            {
                string sql = @"SELECT * 
                                FROM dashboard.dbo.menu
                                WHERE mnuActive = 1
                                    AND mnuParent = 0 
                                    AND mnuLocation = (0) 
                                ORDER BY mnuLocation, mnuDesc";
                output = DCL.Menus.RunCSSMenu(string.Format(sql, mnuLoc), constr);
            }
            catch (Exception e)
            {
                output += "<li>" + e.Message + "</li>";
            }
            finally
            {
                output += "</ul>";
            }
            return output;
        }

        public static string GetTabName(string color)
        {
            string output = null;

            switch (color)
            {
                case "blue":
                    output = "panel-primary";
                    break;
                case "red":
                    output = "panel-red";
                    break;
                case "green":
                    output = "panel-green";
                    break;
                case "yellow":
                    output = "panel-yellow";
                    break;
                case "sucess":
                    output = "panel-success";
                    break;
                case "info":
                    output = "panel-info";
                    break;
                case "warning":
                    output = "panel-warning";
                    break;
                case "danger":
                    output = "panel-danger";
                    break;
                default:
                    output = "panel-default";
                    break;
            }

            return output;
        }
    }
}
