﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace DBMC
{
    public class Global_Dash
    {
        public static string GetConnectionString(string db)
        {
            string output = null;
            try
            {
                output = System.Configuration.ConfigurationManager.ConnectionStrings[db].ConnectionString;
            }
            catch (Exception e)
            {
                output = string.Format("*** ERROR in Global_Dash.GetDomainUsers: {0}", e.Message);
            }
            return output;
        }

        public static string GetUserID(string cs, string user)
        {
            string output = "";
            string[] usr = null;
            string domain = null;
            string username = null;

            try
            {
                if (user.Contains("@"))
                {
                    usr = user.Split('@');
                } else
                {
                    usr = user.Split('\\');
                }
                domain = usr[0];
                username = usr[1];
                string sql = string.Format("exec sp_ValidateUserID '{0}', '{1}'", domain, username);
                output = DCL.Data.GetSingleResults(cs, sql).ToUpper();
            }
            catch (Exception e)
            {
                output = string.Format("*** ERROR in Global_Dash.GetDomainUsers: {0}", e.Message);
            }
            return output;
        }

        public static List<string> GetDomainUsers(string domainName)
        {
            // List of strings for your names
            List<string> output = new List<string>();
            try
            {
                // create your domain context and define the OU container to search in
                System.DirectoryServices.AccountManagement.PrincipalContext ctx =
                        new System.DirectoryServices.AccountManagement.PrincipalContext(
                            System.DirectoryServices.AccountManagement.ContextType.Domain, domainName);

                // define a "query-by-example" principal - here, we search for a UserPrincipal (user)
                System.DirectoryServices.AccountManagement.UserPrincipal qbeUser =
                        new System.DirectoryServices.AccountManagement.UserPrincipal(ctx);

                // create your principal searcher passing in the QBE principal    
                System.DirectoryServices.AccountManagement.PrincipalSearcher srch =
                        new System.DirectoryServices.AccountManagement.PrincipalSearcher(qbeUser);

                // find all matches
                foreach (var found in srch.FindAll())
                {
                    // do whatever here - "found" is of type "Principal" - it could be user, group, computer.....          
                    output.Add(found.DisplayName);
                }
            }
            catch (Exception e)
            {
                output.Add(string.Format("*** ERROR in Global_Dash.GetDomainUsers: {0}", e.Message));
            }
            return output;
        }

        public string CreatePasswordHash(string password)
        {
            string output = "";
            try
            {
                using (SHA512 sha512Hash = SHA512.Create())
                {
                    //From String to byte array
                    string hash = BitConverter.ToString(sha512Hash.ComputeHash(Encoding.UTF8.GetBytes(password))).Replace("-", String.Empty);
                    output = hash;
                }
            }
            catch (Exception e)
            {
                output = e.Message;
            }
            return output;
        }
    }
}
