USE [master]
GO

/****** Object:  UserDefinedFunction [dbo].[udf_StripNonNumerics]    Script Date: 2/13/2019 11:50:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 15 AUG 2018
-- Description:	This function remotes nonnumerics from a field
-- =============================================
CREATE FUNCTION [dbo].[udf_StripNonNumerics]
(
  @Temp nvarchar(MAX)
)
RETURNS nvarchar(MAX)
AS
Begin
	Declare @KeepValues as varchar(50)
	SET @KeepValues = '%[^0-9]%'
	While PatIndex(@KeepValues, @Temp) > 0
		Set @Temp = Stuff(@Temp, PatIndex(@KeepValues, @Temp), 1, '')
	RETURN @Temp
End
GO

