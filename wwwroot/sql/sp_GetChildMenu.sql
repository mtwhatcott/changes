USE [DASHBOARD]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetChildMenu]    Script Date: 2/6/2019 10:36:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 12 DEC 2018
-- Description:	This stored procedure gets menu  
--				child items
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetChildMenu]
	@id INT,
	@parents INT
AS
BEGIN
	SET NOCOUNT ON;
	SET @parents += 1;
	SELECT  m.mnuID, 
		ISNULL(m.mnuDesc,'') 'mnuDesc',
		ISNULL(m.mnuURL,'') 'mnuURL', 
		ISNULL(m.mnuClass, '') 'mnuClass', 
		ISNULL(IIF((dbo.udf_haschildren(m.mnuID) > 0), (SELECT COUNT(*) FROM dashboard.dbo.menu mm WHERE mm.mnuParent=m.mnuID), 0), 0) mnuChildCount, 
		@parents 'mnuLevel',
		ISNULL(m.mnuParent, '') 'mnuParent'
	FROM dashboard.dbo.menu m
	WHERE mnuActive = 1
		AND mnuParent = @id  
	ORDER BY mnuPosition, mnuDesc;

	--SELECT m.mnuID, '<li><a href="' + 
	--	ISNULL(m.mnuURL,'') + 
	--	'"><i class="' + 
	--	ISNULL(m.mnuClass,'') + 
	--	'"></i> ' + 
	--	ISNULL(m.mnuDesc,'') + 
	--	IIF(dbo.udf_haschildren(m.mnuID) > 0, '<span class="fa arrow"></span></a><ul class="nav nav-third-level">', '</a></li>') as HTML
	--FROM dashboard.dbo.menu m
	--WHERE mnuActive = 1
	--	AND mnuParent = @id  
	--ORDER BY mnuPosition, mnuDesc;

END
GO

