USE [DASHBOARD]
GO

/****** Object:  UserDefinedFunction [dbo].[udf_hasChildren]    Script Date: 2/6/2019 10:59:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 14 JAN 2019
-- Description:	This function checks to see if   
--				menu item has children.  
-- =============================================
CREATE FUNCTION [dbo].[udf_hasChildren] 
(	
	@id int
)
RETURNS bit 
AS
BEGIN
	DECLARE @Temp bit = 0;
	SELECT @Temp = COUNT(mnuID)
	FROM dashboard.dbo.menu m
	WHERE m.mnuActive = 1
		AND m.mnuParent = @id
	RETURN @Temp
END
GO

