USE [DASHBOARD]
GO

/****** Object:  StoredProcedure [dbo].[sp_ValidateUserID]    Script Date: 2/6/2019 10:38:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 30 JAN 2019
-- Description:	This stored procedure gets the 
--				users GUID and validates their access
-- =============================================
CREATE PROCEDURE [dbo].[sp_ValidateUserID]
	@DOMAIN NVARCHAR(100),
	@USERNAME NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT u.usrID
	FROM dashboard.dbo.[users] u
	WHERE usrActive = 1
		AND usrDomain = @DOMAIN 
		AND usrUserName = @USERNAME;
END
GO

