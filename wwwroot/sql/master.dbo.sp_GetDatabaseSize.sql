USE [master]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetDatabaseSize]    Script Date: 2/13/2019 10:13:37 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		WHATCOTT, MICHAEL
-- Create date: 	10 OCT 2018
-- Description:		This stored procedure gets the database
--			sized and log sizes.
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDatabaseSize] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WITH fs AS (SELECT database_id, type, size FROM sys.master_files)
	SELECT @@SERVERNAME [Server_Name], name [Database_Name],
    	(SELECT [master].[dbo].[udf_FormatBytes](CAST(SUM(size) as bigint), 'KB')
		FROM fs 
		WHERE type = 0 and fs.database_id = db.database_id) Data_File_Size_GB,
    	(SELECT [master].[dbo].[udf_FormatBytes](CAST(SUM(size) as bigint), 'KB')
		FROM fs 
		WHERE type = 1 and fs.database_id = db.database_id) Log_File_Size_GB
	FROM sys.databases db
	ORDER BY 1, 2
END

GO

