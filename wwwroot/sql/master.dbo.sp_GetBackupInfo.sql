USE [master]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetBackupInfo]    Script Date: 2/13/2019 10:11:19 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 26 JULY 2018
-- Description:	This stored procedure gets the latest 
--				backup name, date and sizes.
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetBackupInfo] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- D = Full, I = Differential and L = Log.
	SELECT
		b.server_name as server_name, 
		b.database_name, 
		(SELECT TOP 1 (a.name) + '.bak' 
			FROM msdb.dbo.backupset a 
			WHERE a.database_name = b.database_name
			AND a.type = 'D'
			ORDER BY a.backup_finish_date DESC) AS backup_name,
		MAX(CASE WHEN b.type = 'D' 
			THEN b.backup_finish_date 
			ELSE NULL 
			END) AS LastFullBackup,
		MAX(CASE WHEN b.type = 'I' 
			THEN b.backup_finish_date 
			ELSE NULL 
			END) AS LastDifferential,
		MAX(CASE WHEN b.type = 'L' 
			THEN b.backup_finish_date 
			ELSE NULL 
			END) AS LastLog,
		MAX(CASE WHEN b.type = 'D' 
			THEN [master].[dbo].[udf_FormatBytes](CAST(b.backup_size as bigint), 'Bytes')
			ELSE NULL 
			END) as LastFullSize,
		MAX(CASE WHEN b.type = 'D' 
			THEN [master].[dbo].[udf_FormatBytes](CAST(b.compressed_backup_size as bigint), 'Bytes')
			ELSE NULL 
			END) as LastFullCompressedSize
	FROM msdb.dbo.backupset b
	WHERE database_name NOT IN ('msdb', 'model', 'master', 'tempdb', 'distribution')
	GROUP BY b.server_name, b.database_name
	ORDER BY b.database_name

	SELECT 
		CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server, 
		bs.database_name, 
		MAX(bs.backup_finish_date) AS last_db_backup_date, 
		DATEDIFF(hh, MAX(bs.backup_finish_date), GETDATE()) AS [Backup Age (Hours)] 
	FROM msdb.dbo.backupset bs 
	WHERE bs.type = 'D'  
		AND bs.database_name NOT IN ('master', 'tempdb', 'model', 'msdb', 'distribution')
	GROUP BY bs.database_name 
	HAVING (MAX(bs.backup_finish_date) < DATEADD(hh, - 24, GETDATE()))  
	UNION  
	--Databases without any backup history 
	SELECT      
	   CONVERT(CHAR(100), SERVERPROPERTY('Servername')) AS Server,  
	   s.NAME AS database_name,  
	   NULL AS [Last Data Backup Date],  
	   9999 AS [Backup Age (Hours)]  
	FROM 
	   master.dbo.sysdatabases s 
	   LEFT JOIN msdb.dbo.backupset bs ON s.name  = bs.database_name 
	WHERE bs.database_name IS NULL 
		AND s.name NOT IN ('master', 'tempdb', 'model', 'msdb', 'distribution')
	ORDER BY  
	   bs.database_name 
END
GO

