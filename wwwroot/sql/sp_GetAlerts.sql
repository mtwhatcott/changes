USE [DASHBOARD]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetAlerts]    Script Date: 2/6/2019 10:35:46 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 29 JAN 2019
-- Description:	This stored procedure gets the 
--				users alerts
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetAlerts]
	@USERID NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;	
	DECLARE @GROUPID NVARCHAR(50);

	SELECT @GROUPID = ugm.ugID 
	FROM [DASHBOARD].[dbo].[USERGROUPMEMBERS] ugm  
	WHERE ugm.usrid = @USERID;
	
	SELECT '<li><a href="alerts.aspx?ID=' + 
			CAST(a.alertID AS VARCHAR(50)) + '"><div><i class="fa fa-bell fa-fw"></i>' + a.alertTitle + 
			'<span class="pull-right text-muted small">' + 
			CAST(DATEDIFF(minute, a.alertDate, getdate()) AS VARCHAR(10)) + 
			' minutes ago</span></div></a></li><li class="divider"></li>' AS HTML
	FROM DASHBOARD.dbo.ALERT a
	WHERE a.alertGroup = @GROUPID
	ORDER BY a.alertDate;
END



GO

