USE [DASHBOARD]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetUserList]    Script Date: 2/6/2019 10:37:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 30 JAN 2019
-- Description:	This stored procedure gets the 
--				users listed in the program
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserList]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * 
	FROM vUsers 
	ORDER BY [name]
END
GO

