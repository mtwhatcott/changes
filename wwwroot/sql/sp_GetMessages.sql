USE [DASHBOARD]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetMessages]    Script Date: 2/6/2019 10:37:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 05 DEC 2018
-- Description:	This stored procedure gets the 
--				users messages
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetMessages]
	@ID NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT '<li><a href="Message.aspx?ID=' + CAST(m.msgID AS VARCHAR(50)) + '"><div ><strong>' + m.msgFrom + '</ strong >
		<span class="pull-right text-muted"><em>' + CAST(m.msgDate AS VARCHAR(25)) + '</em></span></div>
		<div>' + m.msgTitle + '...</div>
		</a></li><li class="divider"></li>' [HTML]
    FROM dashboard.dbo.[MESSAGES] m
    WHERE m.msgTo = CAST(@id AS uniqueidentifier)
        AND m.msgActive = 1 
        --AND m.msgDate >= DATEADD(day,-1,getdate())
    ORDER BY m.msgDate ASC 

END
GO

