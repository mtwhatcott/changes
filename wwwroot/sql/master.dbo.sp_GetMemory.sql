USE [master]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetMemory]    Script Date: 2/13/2019 10:15:57 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 19 SEPT 2018
-- Description:	This stored procedure gets the  
--				total memory used on server.
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetMemory]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@@SERVERNAME as 'Server Name',
		dbo.udf_FormatBytes(physical_memory_in_use_kb, 'KB') AS Physical_Memory_in_Use, 
		dbo.udf_FormatBytes(large_page_allocations_kb, 'KB') AS sql_large_page_allocations_MB, 
		dbo.udf_FormatBytes(locked_page_allocations_kb, 'KB') AS sql_locked_page_allocations_MB,
		dbo.udf_FormatBytes(virtual_address_space_reserved_kb, 'KB') AS sql_VAS_reserved_MB, 
		dbo.udf_FormatBytes(virtual_address_space_committed_kb, 'KB') AS sql_VAS_committed_MB, 
		dbo.udf_FormatBytes(virtual_address_space_available_kb, 'KB') AS sql_VAS_available_MB,
		page_fault_count AS sql_page_fault_count,
		CAST(memory_utilization_percentage AS varchar(10)) + '%' AS sql_memory_utilization_percentage, 
		process_physical_memory_low AS sql_process_physical_memory_low, 
		process_virtual_memory_low AS sql_process_virtual_memory_low
	FROM sys.dm_os_process_memory;
END
GO

