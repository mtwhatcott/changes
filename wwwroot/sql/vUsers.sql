USE [DASHBOARD]
GO

/****** Object:  View [dbo].[vUsers]    Script Date: 2/6/2019 11:13:38 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vUsers]
AS
	SELECT u.usrid, usrFirstName + ' ' + usrLastName 'Name', usrEmail 'Email'
	FROM users u
	LEFT JOIN USERGROUPMEMBERS ugm ON u.usrid=ugm.usrID;
GO

