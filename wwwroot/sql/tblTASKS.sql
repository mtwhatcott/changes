USE [DASHBOARD]
GO

/****** Object:  Table [dbo].[TASKS]    Script Date: 2/6/2019 11:08:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TASKS](
	[taskID] [uniqueidentifier] NOT NULL,
	[taskDate] [datetime] NOT NULL,
	[taskStartDate] [datetime] NULL,
	[taskEndDate] [datetime] NULL,
	[taskSubject] [nvarchar](500) NULL,
	[taskStatus] [uniqueidentifier] NULL,
	[taskPriority] [uniqueidentifier] NULL,
	[taskPercent] [float] NULL,
	[taskReminderDate] [date] NULL,
	[taskReminderTime] [time](7) NULL,
	[taskCreator] [uniqueidentifier] NULL,
	[taskOwner] [uniqueidentifier] NULL,
	[taskFollowUp] [datetime] NULL,
	[taskCategory] [uniqueidentifier] NULL,
	[taskDetails] [ntext] NULL,
 CONSTRAINT [PK_TASKS] PRIMARY KEY CLUSTERED 
(
	[taskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskID]  DEFAULT (newid()) FOR [taskID]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskDate]  DEFAULT (getdate()) FOR [taskDate]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskStartDate]  DEFAULT (getdate()) FOR [taskStartDate]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskEndDate]  DEFAULT (getdate()) FOR [taskEndDate]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskPercent]  DEFAULT ((0)) FOR [taskPercent]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskReminderDate]  DEFAULT (getdate()) FOR [taskReminderDate]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskReminderTime]  DEFAULT (getdate()) FOR [taskReminderTime]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskFollowUp]  DEFAULT (getdate()) FOR [taskFollowUp]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_OPTIONS] FOREIGN KEY([taskStatus])
REFERENCES [dbo].[OPTIONS] ([optID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_OPTIONS]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_OPTIONS1] FOREIGN KEY([taskPriority])
REFERENCES [dbo].[OPTIONS] ([optID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_OPTIONS1]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_OPTIONS2] FOREIGN KEY([taskCategory])
REFERENCES [dbo].[OPTIONS] ([optID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_OPTIONS2]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_USER] FOREIGN KEY([taskCreator])
REFERENCES [dbo].[USERS] ([usrID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_USER]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_USER1] FOREIGN KEY([taskOwner])
REFERENCES [dbo].[USERS] ([usrID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_USER1]
GO

