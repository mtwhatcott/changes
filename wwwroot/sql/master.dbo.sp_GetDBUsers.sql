USE [master]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 21 FEB 2019
-- Description:	This stored procedure gets the  
--				users connected to each db.
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDBUsers]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT DB_NAME(dbid) as DBName, 
		   COUNT(dbid) as NumberOfConnections, 
		   loginame as LoginName 
	FROM sys.sysprocesses 
	WHERE dbid > 0 
	GROUP BY dbid, loginame
END
GO

