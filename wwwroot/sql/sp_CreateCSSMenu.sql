USE [DASHBOARD]
GO

/****** Object:  StoredProcedure [dbo].[sp_CreateCSSMenu]    Script Date: 2/6/2019 10:35:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 05 DEC 2018
-- Description:	This stored procedure creates  
--				the user menu
-- =============================================
CREATE PROCEDURE [dbo].[sp_CreateCSSMenu]
	@LOC NVARCHAR(10)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Id NVARCHAR(MAX);
	DECLARE @HTML NVARCHAR(MAX);
	DECLARE @tmpTABLE TABLE (
		id INT,
		html NVARCHAR(MAX)
	)
	DECLARE @i INT = 0;
	
	DECLARE MY_CURSOR CURSOR 
	  LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR 
	SELECT m.mnuID, IIF(@LOC='top','<li class="dropdown"><ul class="dropdown-menu dropdown-'+ISNULL(LOWER(m.mnuDesc),'')+'">', '') + 
			'<li><a href="' + 
			ISNULL(m.mnuURL,'') + '"><i class="' + 
			ISNULL(m.mnuClass,'') + '"></i> ' + 
			ISNULL(m.mnuDesc,'') + 
			IIF(dbo.udf_haschildren(m.mnuID) > 0, '<span class="fa arrow"></span></a><ul class="nav nav-second-level">', IIF(@LOC='top','</a></li></ul></li>', '</a></li>')) as HTML
		FROM dashboard.dbo.menu m
		WHERE m.mnuActive = 1
			AND m.mnuLocation = @LOC
			AND m.mnuParent = 0

	OPEN MY_CURSOR
	FETCH NEXT FROM MY_CURSOR INTO @Id, @HTML
	WHILE @@FETCH_STATUS = 0
	BEGIN 
		--Do something with Id here
		INSERT INTO @tmpTABLE (id, html) SELECT @Id, @HTML;
		
		IF dbo.udf_haschildren(@Id) > 0
		BEGIN
			DECLARE @dd NVARCHAR(MAX);
			DECLARE @link NVARCHAR(MAX);
			DECLARE MY_CURSOR2 CURSOR 
			  LOCAL STATIC READ_ONLY FORWARD_ONLY
			FOR 
			SELECT mm.mnuID, '<li><a href="' + 
				ISNULL(mm.mnuURL,'') + '"><i class="' + 
				ISNULL(mm.mnuClass,'') + '"></i> ' + 
				ISNULL(mm.mnuDesc,'') + 
				IIF(dbo.udf_haschildren(mm.mnuID) > 0, '<span class="fa arrow"></span></a><ul class="nav nav-third-level">', IIF(@LOC='top','</a></li></ul></li>', '</a></li>')) as HTML
			FROM dashboard.dbo.menu mm
			WHERE mm.mnuActive = 1
				AND mm.mnuParent = @Id

			OPEN MY_CURSOR2
			FETCH NEXT FROM MY_CURSOR2 INTO @dd, @link
			WHILE @@FETCH_STATUS = 0
			BEGIN 
				INSERT INTO @tmpTABLE (id, html) SELECT @dd, @link;

				IF dbo.udf_haschildren(@dd) > 0
				BEGIN
					DECLARE @aa NVARCHAR(MAX);
					DECLARE @output NVARCHAR(MAX);
					DECLARE MY_CURSOR3 CURSOR 
					  LOCAL STATIC READ_ONLY FORWARD_ONLY
					FOR 
					SELECT mmm.mnuID, '<li><a href="' +
						ISNULL(mmm.mnuURL,'') + '"><i class="' + 
						ISNULL(mmm.mnuClass,'') + '"></i> ' + 
						ISNULL(mmm.mnuDesc,'') + 
						IIF(dbo.udf_haschildren(mmm.mnuID) > 0, '<span class="fa arrow"></span></a><ul class="nav nav-fourth-level">', IIF(@LOC='top','</a></li></ul></li>', '</a></li>')) as HTML
					FROM dashboard.dbo.menu mmm
					WHERE mmm.mnuActive = 1
						AND mmm.mnuParent = @dd

					OPEN MY_CURSOR3
					FETCH NEXT FROM MY_CURSOR3 INTO @aa, @output
					WHILE @@FETCH_STATUS = 0
					BEGIN 
						INSERT INTO @tmpTABLE (id, html) SELECT @aa, @output;

						IF dbo.udf_haschildren(@aa) > 0
						BEGIN
							DECLARE @id4 NVARCHAR(MAX);
							DECLARE @html4 NVARCHAR(MAX);
							DECLARE MY_CURSOR4 CURSOR 
							  LOCAL STATIC READ_ONLY FORWARD_ONLY
							FOR 
							SELECT mmmm.mnuID, '<li><a href="' +
								ISNULL(mmmm.mnuURL,'') + '"><i class="' + 
								ISNULL(mmmm.mnuClass,'') + '"></i> ' + 
								ISNULL(mmmm.mnuDesc,'') + 
								IIF(dbo.udf_haschildren(mmmm.mnuID) > 0, '<span class="fa arrow"></span></a><ul class="nav nav-fifth-level">', IIF(@LOC='top','</a></li></ul></li>', '</a></li>')) as HTML
							FROM dashboard.dbo.menu mmmm
							WHERE mmmm.mnuActive = 1
								AND mmmm.mnuParent = @aa

							OPEN MY_CURSOR4
							FETCH NEXT FROM MY_CURSOR4 INTO @id4, @html4
							WHILE @@FETCH_STATUS = 0
							BEGIN 
								INSERT INTO @tmpTABLE (id, html) SELECT @id4, @html4;

								FETCH NEXT FROM MY_CURSOR4 INTO @id4, @html4
							END
							CLOSE MY_CURSOR4
							DEALLOCATE MY_CURSOR4
							INSERT INTO @tmpTABLE (id, html) SELECT @id4, '</ul></li>'; 
						END
						FETCH NEXT FROM MY_CURSOR3 INTO @aa, @output
					END
					CLOSE MY_CURSOR3
					DEALLOCATE MY_CURSOR3
					INSERT INTO @tmpTABLE (id, html) SELECT @aa, '</ul></li>'; 
				END
				FETCH NEXT FROM MY_CURSOR2 INTO @dd, @link
			END
			CLOSE MY_CURSOR2
			DEALLOCATE MY_CURSOR2
			INSERT INTO @tmpTABLE (id, html) SELECT @dd, '</ul></li>';
		END
		FETCH NEXT FROM MY_CURSOR INTO @Id, @HTML
	END
	CLOSE MY_CURSOR
	DEALLOCATE MY_CURSOR
	INSERT INTO @tmpTABLE	(id, html) SELECT @Id, '</ul></li>';

	SELECT html
	FROM @tmpTABLE;
END
GO

