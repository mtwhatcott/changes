USE [DASHBOARD]
GO

/****** Object:  Table [dbo].[USERS]    Script Date: 2/6/2019 11:10:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[USERS](
	[usrID] [uniqueidentifier] NOT NULL,
	[usrDomain] [nvarchar](50) NOT NULL,
	[usrUserName] [nvarchar](50) NOT NULL,
	[usrFirstName] [nvarchar](100) NULL,
	[usrMiddleInitial] [nvarchar](1) NULL,
	[usrLastName] [nvarchar](200) NULL,
	[usrEmail] [nvarchar](500) NOT NULL,
	[usrLevel] [int] NOT NULL,
	[usrCreated] [datetime] NOT NULL,
	[usrActive] [bit] NOT NULL,
 CONSTRAINT [PK_USER] PRIMARY KEY CLUSTERED 
(
	[usrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[USERS] ADD  CONSTRAINT [DF_USER_usrID]  DEFAULT (newid()) FOR [usrID]
GO

ALTER TABLE [dbo].[USERS] ADD  CONSTRAINT [DF_USER_usrLevel]  DEFAULT ((0)) FOR [usrLevel]
GO

ALTER TABLE [dbo].[USERS] ADD  CONSTRAINT [DF_USER_usrCreated]  DEFAULT (getdate()) FOR [usrCreated]
GO

ALTER TABLE [dbo].[USERS] ADD  CONSTRAINT [DF_USER_usrActive]  DEFAULT ((1)) FOR [usrActive]
GO

