USE [master]
GO

/****** Object:  UserDefinedFunction [dbo].[udf_getChildMenus]    Script Date: 2/13/2019 11:49:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 14 JAN 2019
-- Description:	This function gets the child menu  
--				items to display.  
-- =============================================
CREATE FUNCTION [dbo].[udf_getChildMenus] 
(	
	@id int
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT '<li><a href="' + m.[mnuURL] + '"><i class="' + m.[mnuClass] + '"></i> ' + m.[mnuDesc] + '</a></li>' as HTML, mnuID
	FROM dashboard.dbo.menu m
	WHERE m.mnuActive = 1
		AND m.mnuParent = @id
)
GO

