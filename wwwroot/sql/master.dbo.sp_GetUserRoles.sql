USE [master]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 21 FEB 2019
-- Description:	This stored procedure gets the  
--				privileged users and their roles.
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUserRoles]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT rp.name [DB_role], mp.name [DB_user]
	FROM sys.server_role_members drm
	JOIN sys.server_principals rp ON (drm.role_principal_id = rp.principal_id)
	JOIN sys.server_principals mp ON (drm.member_principal_id = mp.principal_id)
	ORDER BY 1,2;
END
GO

