USE [DASHBOARD]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetTasks]    Script Date: 2/6/2019 10:37:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 05 DEC 2018
-- Description:	This stored procedure gets the 
--				users tasks
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetTasks]
	@ID NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT '<li><a href="tasks.aspx?ID=' + 
			CAST(t.taskID AS VARCHAR(50)) + '"><div><p><strong>' + t.taskSubject + 
			'</strong><span class="pull-right text-muted">' + 
			CAST(t.taskPercent AS VARCHAR(10)) + 
			' % Complete</span></p>
			</div><div class="progress progress-striped active"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="' + 
			CAST(t.taskPercent AS VARCHAR(10)) +
			'" aria-valuemin="0" aria-valuemax="100" style="width: ' + 
			CAST(t.taskPercent AS VARCHAR(10)) + 
			'%"><span class="sr-only"> ' + 
			CAST(t.taskPercent AS VARCHAR(10)) + 
			'% Complete (success)</span></div></div></a></li><li class="divider"></li>' AS HTML
	FROM dashboard.dbo.tasks t
	WHERE t.taskOwner = @ID
		AND t.taskPercent < 99.99
	ORDER BY t.taskDate;
END
GO

