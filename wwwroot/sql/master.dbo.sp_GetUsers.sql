USE [master]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetUsers]    Script Date: 2/13/2019 10:33:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 19 SEPT 2018
-- Description:	This stored procedure gets the latest 
--				users active or inactive.
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetUsers]
	-- Add the parameters for the stored procedure here
	@active bit = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@@SERVERNAME as 'Server Name',
		name AS 'User'
		, PRINCIPAL_ID
		, type AS 'User Type'
		, type_desc AS 'Login Type'
		, CAST(create_date AS DATE) AS 'Date Created' 
		, default_database_name AS 'Database Name'
		, IIF(is_disabled LIKE 0, 'No', 'Yes') AS 'Is Active'
	FROM master.sys.server_principals
	WHERE type LIKE 's' 
		OR type LIKE 'u' 
		AND is_disabled = @active				
	ORDER BY [Is Active], [User], [Database Name];
END
GO

