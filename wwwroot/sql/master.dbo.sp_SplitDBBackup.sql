USE [master]
GO

/****** Object:  StoredProcedure [dbo].[sp_SplitDBBackup]    Script Date: 2/13/2019 10:35:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:		Whatcott, Michael
-- Create date: 02 OCT 2018
-- Description:
--           The procedure takes 3 parameters:
--            1. @dbName = Logical database name
--            2. @backupDir = backup directory to store all backup parts
--            3. @nParts = number of files for which to split the backup.
--           
--           The procedure splits the database backup files to the number of files 
--           given each file is numbered and sized as equal as possible.
--           All the backup parts are stored in the Backup directory.
-- =================================================================================================
CREATE PROCEDURE [dbo].[sp_SplitDBBackup] (
   @dbName NVARCHAR(200),
   @backupDir NVARCHAR (200),
   @nParts TINYINT)
AS
BEGIN
   SET NOCOUNT ON
   DECLARE @backupTSQLCmd NVARCHAR (2000)
   DECLARE @idx TINYINT = 0
   DECLARE @fileDate VARCHAR (20) -- used for file name
   DECLARE @Cert NVARCHAR (200) -- used for certificates
   DECLARE @fName NVARCHAR (200) -- file Name
   DECLARE @fileName NVARCHAR (200) -- File Path Name
   SET @idx += 1
   SELECT @fileDate = CONVERT(VARCHAR(20),GETDATE(),112) + '_' + REPLACE(CONVERT(VARCHAR(20),GETDATE(),108),':','');
   SELECT @Cert = (SELECT name FROM sys.certificates WHERE pvt_key_encryption_type_desc = 'ENCRYPTED_BY_MASTER_KEY');
   SET @backupTSQLCmd = 'BACKUP DATABASE ' + @dbName + ' TO '
 
   WHILE @idx <= @nParts
   BEGIN
	  SET @fName = @dbName + '_' + @fileDate + '_Part' + rtrim(ltrim(STR(@idx))) + '.BAK'
      SET @fileName = @backupDir + '\' + @dbName + '\' + @fName 
      SET @backupTSQLCmd += 'DISK = ''' + @fileName + ''', '
      SET @idx += 1
   END
   SET @backupTSQLCmd = left (@backupTSQLCmd, len(@backupTSQLCmd) - 1)
   SET @backupTSQLCmd += ' WITH FORMAT, INIT, COMPRESSION, ENCRYPTION ( ALGORITHM = AES_256, SERVER CERTIFICATE = ' + 
							@Cert + ' ), MEDIANAME = ''' + @filename + ''', NAME = ''' + @fName + '''; '
   PRINT @backupTSQLCmd
   EXEC (@backupTSQLCmd)
   SET NOCOUNT OFF
END
GO

