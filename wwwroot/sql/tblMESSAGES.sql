USE [DASHBOARD]
GO

/****** Object:  Table [dbo].[MESSAGES]    Script Date: 2/6/2019 11:06:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MESSAGES](
	[msgID] [uniqueidentifier] NOT NULL,
	[msgDate] [datetime] NOT NULL,
	[msgTo] [uniqueidentifier] NULL,
	[msgFrom] [nvarchar](500) NULL,
	[msgTitle] [nvarchar](100) NULL,
	[msgText] [ntext] NULL,
	[msgActive] [bit] NOT NULL,
 CONSTRAINT [PK_MESSAGES] PRIMARY KEY CLUSTERED 
(
	[msgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[MESSAGES] ADD  CONSTRAINT [DF_MESSAGES_msgID]  DEFAULT (newid()) FOR [msgID]
GO

ALTER TABLE [dbo].[MESSAGES] ADD  CONSTRAINT [DF_MESSAGES_msgDate]  DEFAULT (getdate()) FOR [msgDate]
GO

ALTER TABLE [dbo].[MESSAGES] ADD  CONSTRAINT [DF_MESSAGES_msgActive]  DEFAULT ((1)) FOR [msgActive]
GO

ALTER TABLE [dbo].[MESSAGES]  WITH CHECK ADD  CONSTRAINT [FK_MESSAGES_USER] FOREIGN KEY([msgTo])
REFERENCES [dbo].[USERS] ([usrID])
GO

ALTER TABLE [dbo].[MESSAGES] CHECK CONSTRAINT [FK_MESSAGES_USER]
GO

