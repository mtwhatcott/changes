USE [master]
GO

/****** Object:  StoredProcedure [dbo].[sp_CurrentLogins]    Script Date: 2/13/2019 10:10:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 19 NOV 2018
-- Description:	This stored procedure gets the  
--				users currently logged into the database.
-- =============================================
CREATE PROCEDURE [dbo].[sp_CurrentLogins]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @t1 as table (
		spid int, [status] nvarchar(50), 
		[login] nvarchar(50), 
		hostname nvarchar(100), 
		blkby varchar(50), 
		dbname nvarchar(100), 
		command nvarchar(max), 
		cuptime int, 
		diskio int, 
		lastbatch nvarchar(50), 
		programname nvarchar(max), 
		spid1 int, 
		requestid int
	)
	INSERT INTO @t1 EXEC sp_who2
	SELECT spid 'SPID', 
		[status] 'STATUS', 
		[login] 'LOGIN', 
		hostname 'HOST NAME', 
		dbname 'DB NAME', 
		command 'COMMAND', 
		programname 'PROGRAM NAME'
	FROM @t1
	--WHERE dbname NOT IN ('master', 'tempdb', 'model', 'msdb', 'distribution')
	ORDER BY dbname, spid;
END
GO

