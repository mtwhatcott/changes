USE [master]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetDatabaseRowCounts]    Script Date: 2/13/2019 10:12:22 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 26 JULY 2018
-- Description:	This stored procedure gets the 
--				Row count for each database and tables
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetDatabaseRowCounts] 
	@db nvarchar(50) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @tmp table ([servername] nvarchar(max), [database] nvarchar(max), tablename nvarchar(max), rundate datetime, [rowcount] bigint)
	DECLARE @query nvarchar(max) 
	IF @db IS NULL
	BEGIN
		SELECT @query = 'IF ''?'' IN (''distribution'', ''master'', ''model'', ''msdb'', ''tempdb'') RETURN
			USE [?] SELECT @@SERVERNAME as [servername], ''?'' as [database], QUOTENAME(SCHEMA_NAME(s.schema_id)) + ''.'' + QUOTENAME(s.name) AS [TableName], getdate() [Run Date], SUM(p.Rows) AS [RowCount]
		FROM sys.objects AS s
			INNER JOIN sys.partitions AS p ON s.object_id = p.object_id
		WHERE s.type = ''U'' 
			AND s.is_ms_shipped = 0x0
			AND index_id < 2 -- 0:Heap, 1:Clustered
		GROUP BY s.schema_id, s.name';
	END
	ELSE
	BEGIN
		SELECT @query = 'IF ''?'' <> ''' + @db + ''' RETURN
			USE [?] SELECT @@SERVERNAME as [servername], ''?'' as [database], QUOTENAME(SCHEMA_NAME(s.schema_id)) + ''.'' + QUOTENAME(s.name) AS [TableName], getdate() [Run Date], SUM(p.Rows) AS [RowCount]
		FROM sys.objects AS s
			INNER JOIN sys.partitions AS p ON s.object_id = p.object_id
		WHERE s.type = ''U'' 
			AND s.is_ms_shipped = 0x0
			AND index_id < 2 -- 0:Heap, 1:Clustered
		GROUP BY s.schema_id, s.name';
	END

	INSERT INTO @tmp EXEC sp_MSforeachdb @query

	SELECT ServerName, [Database], TableName, RunDate, FORMAT([RowCount], '###,###') AS [RowCount] 
	FROM @tmp 
	ORDER BY servername, [database], tablename;
END
GO

