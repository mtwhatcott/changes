USE [DASHBOARD]
GO

/****** Object:  Table [dbo].[USERROLES]    Script Date: 2/6/2019 11:09:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[USERROLES](
	[roleID] [uniqueidentifier] NOT NULL,
	[roleType] [nchar](10) NULL,
	[roleName] [nvarchar](100) NOT NULL,
	[rolePermissions] [nvarchar](100) NULL,
 CONSTRAINT [PK_USERROLES] PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[USERROLES] ADD  CONSTRAINT [DF_USERROLES_roleID]  DEFAULT (newid()) FOR [roleID]
GO

