USE [master]
GO

/****** Object:  StoredProcedure [dbo].[sp_GetRunningBackup]    Script Date: 2/13/2019 10:17:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 15 AUG 2018
-- Description:	This stored procedure gets the current 
--				running backup or restore procedure.
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetRunningBackup] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT command,
		s.text,
		start_time,
		percent_complete, 
		CAST(((DATEDIFF(s,start_time,GetDate()))/3600) as varchar) + ' hour(s), '
				+ CAST((DATEDIFF(s,start_time,GetDate())%3600)/60 as varchar) + 'min, '
				+ CAST((DATEDIFF(s,start_time,GetDate())%60) as varchar) + ' sec' as running_time,
		CAST((estimated_completion_time/3600000) as varchar) + ' hour(s), '
				+ CAST((estimated_completion_time %3600000)/60000 as varchar) + 'min, '
				+ CAST((estimated_completion_time %60000)/1000 as varchar) + ' sec' as est_time_to_go,
		dateadd(second,estimated_completion_time/1000, getdate()) as est_completion_time 
	FROM sys.dm_exec_requests r
		CROSS APPLY sys.dm_exec_sql_text(r.sql_handle) s
	WHERE r.command in ('RESTORE DATABASE', 'BACKUP DATABASE', 'RESTORE LOG', 'BACKUP LOG')
END
GO

