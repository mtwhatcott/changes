USE [DASHBOARD]
GO

/****** Object:  Table [dbo].[MENU]    Script Date: 2/6/2019 11:05:12 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MENU](
	[mnuID] [int] IDENTITY(1,1) NOT NULL,
	[mnuDesc] [nvarchar](50) NOT NULL,
	[mnuParent] [int] NULL,
	[mnuActive] [bit] NOT NULL,
	[mnuDateAdded] [datetime] NOT NULL,
	[mnuLocation] [nchar](10) NOT NULL,
	[mnuURL] [nvarchar](500) NOT NULL,
	[mnuPosition] [int] NOT NULL,
	[mnuClass] [nvarchar](50) NULL,
 CONSTRAINT [PK_MENU] PRIMARY KEY CLUSTERED 
(
	[mnuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuActive]  DEFAULT ((1)) FOR [mnuActive]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuDateAdded]  DEFAULT (getdate()) FOR [mnuDateAdded]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuLocation]  DEFAULT (N'SIDE') FOR [mnuLocation]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuURL]  DEFAULT (N'#') FOR [mnuURL]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuPosition]  DEFAULT ((0)) FOR [mnuPosition]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuClass]  DEFAULT (N'fa fa-dashboard fa-fw') FOR [mnuClass]
GO

