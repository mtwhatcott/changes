USE [master]
GO

/****** Object:  Database [DASHBOARD]    Script Date: 2/6/2019 11:01:06 AM ******/
CREATE DATABASE [DASHBOARD]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DASHBOARD', FILENAME = N'F:\DATA\DASHBOARD.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DASHBOARD_log', FILENAME = N'G:\LOGS\DASHBOARD_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [DASHBOARD] SET COMPATIBILITY_LEVEL = 130
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DASHBOARD].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [DASHBOARD] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [DASHBOARD] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [DASHBOARD] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [DASHBOARD] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [DASHBOARD] SET ARITHABORT OFF 
GO

ALTER DATABASE [DASHBOARD] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [DASHBOARD] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [DASHBOARD] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [DASHBOARD] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [DASHBOARD] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [DASHBOARD] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [DASHBOARD] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [DASHBOARD] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [DASHBOARD] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [DASHBOARD] SET  DISABLE_BROKER 
GO

ALTER DATABASE [DASHBOARD] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [DASHBOARD] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [DASHBOARD] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [DASHBOARD] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [DASHBOARD] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [DASHBOARD] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [DASHBOARD] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [DASHBOARD] SET RECOVERY FULL 
GO

ALTER DATABASE [DASHBOARD] SET  MULTI_USER 
GO

ALTER DATABASE [DASHBOARD] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [DASHBOARD] SET DB_CHAINING OFF 
GO

ALTER DATABASE [DASHBOARD] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [DASHBOARD] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [DASHBOARD] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [DASHBOARD] SET QUERY_STORE = OFF
GO

USE [DASHBOARD]
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE [DASHBOARD] SET  READ_WRITE 
GO

USE [DASHBOARD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ALERT](
	[alertID] [uniqueidentifier] NOT NULL,
	[alertType] [nvarchar](50) NULL,
	[alertTitle] [nvarchar](50) NOT NULL,
	[alertMessage] [text] NOT NULL,
	[alertURL] [nvarchar](max) NULL,
	[alertDate] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
	[alertGroup] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ALERT] PRIMARY KEY CLUSTERED 
(
	[alertID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ALERT] ADD  CONSTRAINT [DF_ALERT_alertID]  DEFAULT (newid()) FOR [alertID]
GO

ALTER TABLE [dbo].[ALERT] ADD  CONSTRAINT [DF_ALERT_alertDate]  DEFAULT (getdate()) FOR [alertDate]
GO

ALTER TABLE [dbo].[ALERT] ADD  CONSTRAINT [DF_ALERT_active]  DEFAULT ((1)) FOR [active]
GO

ALTER TABLE [dbo].[ALERT]  WITH CHECK ADD  CONSTRAINT [FK_ALERT_USERGROUP] FOREIGN KEY([alertGroup])
REFERENCES [dbo].[USERGROUP] ([ugID])
GO

ALTER TABLE [dbo].[ALERT] CHECK CONSTRAINT [FK_ALERT_USERGROUP]
GO

CREATE TABLE [dbo].[MENU](
	[mnuID] [int] IDENTITY(1,1) NOT NULL,
	[mnuDesc] [nvarchar](50) NOT NULL,
	[mnuParent] [int] NULL,
	[mnuActive] [bit] NOT NULL,
	[mnuDateAdded] [datetime] NOT NULL,
	[mnuLocation] [nchar](10) NOT NULL,
	[mnuURL] [nvarchar](500) NOT NULL,
	[mnuPosition] [int] NOT NULL,
	[mnuClass] [nvarchar](50) NULL,
 CONSTRAINT [PK_MENU] PRIMARY KEY CLUSTERED 
(
	[mnuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuActive]  DEFAULT ((1)) FOR [mnuActive]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuDateAdded]  DEFAULT (getdate()) FOR [mnuDateAdded]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuLocation]  DEFAULT (N'SIDE') FOR [mnuLocation]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuURL]  DEFAULT (N'#') FOR [mnuURL]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuPosition]  DEFAULT ((0)) FOR [mnuPosition]
GO

ALTER TABLE [dbo].[MENU] ADD  CONSTRAINT [DF_MENU_mnuClass]  DEFAULT (N'fa fa-dashboard fa-fw') FOR [mnuClass]
GO

CREATE TABLE [dbo].[MENUGROUPS](
	[mgID] [uniqueidentifier] NOT NULL,
	[mnuID] [int] NOT NULL,
	[ugID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_MENUGROUPS] PRIMARY KEY CLUSTERED 
(
	[mgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MENUGROUPS] ADD  CONSTRAINT [DF_MENUGROUPS_mgID]  DEFAULT (newid()) FOR [mgID]
GO

ALTER TABLE [dbo].[MENUGROUPS]  WITH CHECK ADD  CONSTRAINT [FK_MENUGROUPS_MENU] FOREIGN KEY([mnuID])
REFERENCES [dbo].[MENU] ([mnuID])
GO

ALTER TABLE [dbo].[MENUGROUPS] CHECK CONSTRAINT [FK_MENUGROUPS_MENU]
GO

ALTER TABLE [dbo].[MENUGROUPS]  WITH CHECK ADD  CONSTRAINT [FK_MENUGROUPS_USERGROUP] FOREIGN KEY([ugID])
REFERENCES [dbo].[USERGROUP] ([ugID])
GO

ALTER TABLE [dbo].[MENUGROUPS] CHECK CONSTRAINT [FK_MENUGROUPS_USERGROUP]
GO

CREATE TABLE [dbo].[MESSAGES](
	[msgID] [uniqueidentifier] NOT NULL,
	[msgDate] [datetime] NOT NULL,
	[msgTo] [uniqueidentifier] NULL,
	[msgFrom] [nvarchar](500) NULL,
	[msgTitle] [nvarchar](100) NULL,
	[msgText] [ntext] NULL,
	[msgActive] [bit] NOT NULL,
 CONSTRAINT [PK_MESSAGES] PRIMARY KEY CLUSTERED 
(
	[msgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[MESSAGES] ADD  CONSTRAINT [DF_MESSAGES_msgID]  DEFAULT (newid()) FOR [msgID]
GO

ALTER TABLE [dbo].[MESSAGES] ADD  CONSTRAINT [DF_MESSAGES_msgDate]  DEFAULT (getdate()) FOR [msgDate]
GO

ALTER TABLE [dbo].[MESSAGES] ADD  CONSTRAINT [DF_MESSAGES_msgActive]  DEFAULT ((1)) FOR [msgActive]
GO

ALTER TABLE [dbo].[MESSAGES]  WITH CHECK ADD  CONSTRAINT [FK_MESSAGES_USER] FOREIGN KEY([msgTo])
REFERENCES [dbo].[USERS] ([usrID])
GO

ALTER TABLE [dbo].[MESSAGES] CHECK CONSTRAINT [FK_MESSAGES_USER]
GO

CREATE TABLE [dbo].[OPTIONS](
	[optID] [uniqueidentifier] NOT NULL,
	[optDescription] [nvarchar](100) NULL,
	[optTable] [nvarchar](50) NULL,
	[optCreateDate] [datetime] NOT NULL,
	[optDefaultSelect] [bit] NOT NULL,
 CONSTRAINT [PK_OPTIONS] PRIMARY KEY CLUSTERED 
(
	[optID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[OPTIONS] ADD  CONSTRAINT [DF_Table_1_taskOptionID]  DEFAULT (newid()) FOR [optID]
GO

ALTER TABLE [dbo].[OPTIONS] ADD  CONSTRAINT [DF_OPTIONS_optCreateDate]  DEFAULT (getdate()) FOR [optCreateDate]
GO

ALTER TABLE [dbo].[OPTIONS] ADD  CONSTRAINT [DF_OPTIONS_optDefaultSelect]  DEFAULT ((0)) FOR [optDefaultSelect]
GO

CREATE TABLE [dbo].[QUERIES](
	[queryID] [uniqueidentifier] NOT NULL,
	[queryTitle] [nvarchar](100) NOT NULL,
	[query] [text] NOT NULL,
	[queryType] [nvarchar](50) NULL,
	[class] [nvarchar](50) NULL,
	[icon] [nvarchar](50) NULL,
	[location] [nvarchar](50) NULL,
	[url] [nvarchar](100) NULL,
	[queryHTML] [text] NOT NULL,
	[dateadded] [datetime] NOT NULL,
 CONSTRAINT [PK_Queries] PRIMARY KEY CLUSTERED 
(
	[queryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[QUERIES] ADD  CONSTRAINT [DF_Queries_qID]  DEFAULT (newid()) FOR [queryID]
GO

ALTER TABLE [dbo].[QUERIES] ADD  CONSTRAINT [DF_Queries_dateadded]  DEFAULT (getdate()) FOR [dateadded]
GO

CREATE TABLE [dbo].[TASKS](
	[taskID] [uniqueidentifier] NOT NULL,
	[taskDate] [datetime] NOT NULL,
	[taskStartDate] [datetime] NULL,
	[taskEndDate] [datetime] NULL,
	[taskSubject] [nvarchar](500) NULL,
	[taskStatus] [uniqueidentifier] NULL,
	[taskPriority] [uniqueidentifier] NULL,
	[taskPercent] [float] NULL,
	[taskReminderDate] [date] NULL,
	[taskReminderTime] [time](7) NULL,
	[taskCreator] [uniqueidentifier] NULL,
	[taskOwner] [uniqueidentifier] NULL,
	[taskFollowUp] [datetime] NULL,
	[taskCategory] [uniqueidentifier] NULL,
	[taskDetails] [ntext] NULL,
 CONSTRAINT [PK_TASKS] PRIMARY KEY CLUSTERED 
(
	[taskID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskID]  DEFAULT (newid()) FOR [taskID]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskDate]  DEFAULT (getdate()) FOR [taskDate]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskStartDate]  DEFAULT (getdate()) FOR [taskStartDate]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskEndDate]  DEFAULT (getdate()) FOR [taskEndDate]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskPercent]  DEFAULT ((0)) FOR [taskPercent]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskReminderDate]  DEFAULT (getdate()) FOR [taskReminderDate]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskReminderTime]  DEFAULT (getdate()) FOR [taskReminderTime]
GO

ALTER TABLE [dbo].[TASKS] ADD  CONSTRAINT [DF_TASKS_taskFollowUp]  DEFAULT (getdate()) FOR [taskFollowUp]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_OPTIONS] FOREIGN KEY([taskStatus])
REFERENCES [dbo].[OPTIONS] ([optID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_OPTIONS]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_OPTIONS1] FOREIGN KEY([taskPriority])
REFERENCES [dbo].[OPTIONS] ([optID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_OPTIONS1]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_OPTIONS2] FOREIGN KEY([taskCategory])
REFERENCES [dbo].[OPTIONS] ([optID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_OPTIONS2]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_USER] FOREIGN KEY([taskCreator])
REFERENCES [dbo].[USERS] ([usrID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_USER]
GO

ALTER TABLE [dbo].[TASKS]  WITH CHECK ADD  CONSTRAINT [FK_TASKS_USER1] FOREIGN KEY([taskOwner])
REFERENCES [dbo].[USERS] ([usrID])
GO

ALTER TABLE [dbo].[TASKS] CHECK CONSTRAINT [FK_TASKS_USER1]
GO

CREATE TABLE [dbo].[USERGROUP](
	[ugID] [uniqueidentifier] NOT NULL,
	[ugName] [nvarchar](50) NOT NULL,
	[ugType] [nvarchar](50) NULL,
	[ugRoleID] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_USERGROUP] PRIMARY KEY CLUSTERED 
(
	[ugID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[USERGROUP] ADD  CONSTRAINT [DF_Table_1_userGroupID]  DEFAULT (newid()) FOR [ugID]
GO

ALTER TABLE [dbo].[USERGROUP]  WITH CHECK ADD  CONSTRAINT [FK_USERGROUP_USERROLES] FOREIGN KEY([ugRoleID])
REFERENCES [dbo].[USERROLES] ([roleID])
GO

ALTER TABLE [dbo].[USERGROUP] CHECK CONSTRAINT [FK_USERGROUP_USERROLES]
GO

CREATE TABLE [dbo].[USERGROUPMEMBERS](
	[ugmID] [uniqueidentifier] NOT NULL,
	[ugID] [uniqueidentifier] NOT NULL,
	[usrID] [uniqueidentifier] NOT NULL,
	[ugmDateAdded] [datetime] NOT NULL,
 CONSTRAINT [PK_USERGROUPMEMBERS] PRIMARY KEY CLUSTERED 
(
	[ugmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[USERGROUPMEMBERS] ADD  CONSTRAINT [DF_USERGROUPMEMBERS_ugmID]  DEFAULT (newid()) FOR [ugmID]
GO

ALTER TABLE [dbo].[USERGROUPMEMBERS] ADD  CONSTRAINT [DF_USERGROUPMEMBERS_ugmDateAdded]  DEFAULT (getdate()) FOR [ugmDateAdded]
GO

ALTER TABLE [dbo].[USERGROUPMEMBERS]  WITH CHECK ADD  CONSTRAINT [FK_USERGROUPMEMBERS_USER] FOREIGN KEY([usrID])
REFERENCES [dbo].[USERS] ([usrID])
GO

ALTER TABLE [dbo].[USERGROUPMEMBERS] CHECK CONSTRAINT [FK_USERGROUPMEMBERS_USER]
GO

ALTER TABLE [dbo].[USERGROUPMEMBERS]  WITH CHECK ADD  CONSTRAINT [FK_USERGROUPMEMBERS_USERGROUP] FOREIGN KEY([ugID])
REFERENCES [dbo].[USERGROUP] ([ugID])
GO

ALTER TABLE [dbo].[USERGROUPMEMBERS] CHECK CONSTRAINT [FK_USERGROUPMEMBERS_USERGROUP]
GO

CREATE TABLE [dbo].[USERROLES](
	[roleID] [uniqueidentifier] NOT NULL,
	[roleType] [nchar](10) NULL,
	[roleName] [nvarchar](100) NOT NULL,
	[rolePermissions] [nvarchar](100) NULL,
 CONSTRAINT [PK_USERROLES] PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[USERROLES] ADD  CONSTRAINT [DF_USERROLES_roleID]  DEFAULT (newid()) FOR [roleID]
GO

CREATE TABLE [dbo].[USERS](
	[usrID] [uniqueidentifier] NOT NULL,
	[usrDomain] [nvarchar](50) NOT NULL,
	[usrUserName] [nvarchar](50) NOT NULL,
	[usrFirstName] [nvarchar](100) NULL,
	[usrMiddleInitial] [nvarchar](1) NULL,
	[usrLastName] [nvarchar](200) NULL,
	[usrEmail] [nvarchar](500) NOT NULL,
	[usrLevel] [int] NOT NULL,
	[usrCreated] [datetime] NOT NULL,
	[usrActive] [bit] NOT NULL,
 CONSTRAINT [PK_USER] PRIMARY KEY CLUSTERED 
(
	[usrID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[USERS] ADD  CONSTRAINT [DF_USER_usrID]  DEFAULT (newid()) FOR [usrID]
GO

ALTER TABLE [dbo].[USERS] ADD  CONSTRAINT [DF_USER_usrLevel]  DEFAULT ((0)) FOR [usrLevel]
GO

ALTER TABLE [dbo].[USERS] ADD  CONSTRAINT [DF_USER_usrCreated]  DEFAULT (getdate()) FOR [usrCreated]
GO

ALTER TABLE [dbo].[USERS] ADD  CONSTRAINT [DF_USER_usrActive]  DEFAULT ((1)) FOR [usrActive]
GO


CREATE VIEW [dbo].[vGroups]
AS
SELECT        v.usrid AS 'ID', v.Name AS 'NAME', g.ugType AS 'GROUP'
FROM            dbo.vUsers AS v LEFT OUTER JOIN
                         dbo.USERGROUPMEMBERS AS ugm ON v.usrid = ugm.usrID LEFT OUTER JOIN
                         dbo.USERGROUP AS g ON ugm.ugID = g.ugID
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "v"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ugm"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 419
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "g"
            Begin Extent = 
               Top = 120
               Left = 38
               Bottom = 250
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2220
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vGroups'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vGroups'
GO

CREATE VIEW [dbo].[vUsers]
AS
	SELECT u.usrid, usrFirstName + ' ' + usrLastName 'Name', usrEmail 'Email'
	FROM users u
	LEFT JOIN USERGROUPMEMBERS ugm ON u.usrid=ugm.usrID;
GO



