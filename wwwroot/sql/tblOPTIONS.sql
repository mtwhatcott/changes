USE [DASHBOARD]
GO

/****** Object:  Table [dbo].[OPTIONS]    Script Date: 2/6/2019 11:06:41 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OPTIONS](
	[optID] [uniqueidentifier] NOT NULL,
	[optDescription] [nvarchar](100) NULL,
	[optTable] [nvarchar](50) NULL,
	[optCreateDate] [datetime] NOT NULL,
	[optDefaultSelect] [bit] NOT NULL,
 CONSTRAINT [PK_OPTIONS] PRIMARY KEY CLUSTERED 
(
	[optID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[OPTIONS] ADD  CONSTRAINT [DF_Table_1_taskOptionID]  DEFAULT (newid()) FOR [optID]
GO

ALTER TABLE [dbo].[OPTIONS] ADD  CONSTRAINT [DF_OPTIONS_optCreateDate]  DEFAULT (getdate()) FOR [optCreateDate]
GO

ALTER TABLE [dbo].[OPTIONS] ADD  CONSTRAINT [DF_OPTIONS_optDefaultSelect]  DEFAULT ((0)) FOR [optDefaultSelect]
GO

