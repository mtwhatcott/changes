-- =============================================
-- Author:		Whatcott, Michael
-- Create date: 14 FEB 2019
-- Description:	This .sql file creates the Dashboard  
--				database, its tables, views and 
--				stored procedures.
-- =============================================

-- This is the main caller for each script
SET NOCOUNT ON
GO

DECLARE $PATH NVARCHAR(250)='C:\sdcard\OneDrive - Mountain West Consulting Services\CODE\Web\DASHBOARD_DEV\DASHBOARD_DEV\sql';
DECLARE $DBNAME NVARCHAR(100)='DASHBOARD';

PRINT 'CREATING DATABASE $DBNAME'
IF EXISTS (SELECT 1 FROM SYS.DATABASES WHERE NAME = $DBNAME);
DROP DATABASE $DBNAME;
GO

:On Error exit

:r $PATH\Create_Dashboard_Database.sql
:r $PATH\tblALERT.sql
:r $PATH\tblMENU.sql
:r $PATH\tblMENUGROUPS.sql
:r $PATH\tblMESSAGES.sql
:r $PATH\tblOPTIONS.sql
:r $PATH\tblQUERIES.sql
:r $PATH\tblTASKS.sql
:r $PATH\tblUSERGROUP.sql
:r $PATH\tblUSERGROUPMEMBERS.sql
:r $PATH\tblUSERROLES.sql
:r $PATH\tblUSERS.sql
:r $PATH\vGroups.sql
:r $PATH\vUsers.sql
:r $PATH\udf_hasChildren.sql
:r $PATH\sp_CreateCSSMenu.sql
:r $PATH\sp_GetAlerts.sql
:r $PATH\sp_GetChildMenu.sql
:r $PATH\sp_GetMenu.sql
:r $PATH\sp_GetMessages.sql
:r $PATH\sp_GetTasks.sql
:r $PATH\sp_GetUserList.sql
:r $PATH\sp_ValidateUserID.sql

PRINT 'CREATION OF DATAVASE $dbname AND ITS TABLES HAS COMPLETED'
GO

