USE [DASHBOARD]
GO

/****** Object:  Table [dbo].[QUERIES]    Script Date: 2/6/2019 11:07:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[QUERIES](
	[queryID] [uniqueidentifier] NOT NULL,
	[queryTitle] [nvarchar](100) NOT NULL,
	[query] [text] NOT NULL,
	[queryType] [nvarchar](50) NULL,
	[class] [nvarchar](50) NULL,
	[icon] [nvarchar](50) NULL,
	[location] [nvarchar](50) NULL,
	[url] [nvarchar](100) NULL,
	[queryHTML] [text] NOT NULL,
	[dateadded] [datetime] NOT NULL,
 CONSTRAINT [PK_Queries] PRIMARY KEY CLUSTERED 
(
	[queryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[QUERIES] ADD  CONSTRAINT [DF_Queries_qID]  DEFAULT (newid()) FOR [queryID]
GO

ALTER TABLE [dbo].[QUERIES] ADD  CONSTRAINT [DF_Queries_dateadded]  DEFAULT (getdate()) FOR [dateadded]
GO

