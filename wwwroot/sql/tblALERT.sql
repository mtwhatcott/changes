USE [DASHBOARD]
GO

/****** Object:  Table [dbo].[ALERT]    Script Date: 2/6/2019 11:04:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ALERT](
	[alertID] [uniqueidentifier] NOT NULL,
	[alertType] [nvarchar](50) NULL,
	[alertTitle] [nvarchar](50) NOT NULL,
	[alertMessage] [text] NOT NULL,
	[alertURL] [nvarchar](max) NULL,
	[alertDate] [datetime] NOT NULL,
	[active] [bit] NOT NULL,
	[alertGroup] [uniqueidentifier] NULL,
 CONSTRAINT [PK_ALERT] PRIMARY KEY CLUSTERED 
(
	[alertID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[ALERT] ADD  CONSTRAINT [DF_ALERT_alertID]  DEFAULT (newid()) FOR [alertID]
GO

ALTER TABLE [dbo].[ALERT] ADD  CONSTRAINT [DF_ALERT_alertDate]  DEFAULT (getdate()) FOR [alertDate]
GO

ALTER TABLE [dbo].[ALERT] ADD  CONSTRAINT [DF_ALERT_active]  DEFAULT ((1)) FOR [active]
GO

ALTER TABLE [dbo].[ALERT]  WITH CHECK ADD  CONSTRAINT [FK_ALERT_USERGROUP] FOREIGN KEY([alertGroup])
REFERENCES [dbo].[USERGROUP] ([ugID])
GO

ALTER TABLE [dbo].[ALERT] CHECK CONSTRAINT [FK_ALERT_USERGROUP]
GO

